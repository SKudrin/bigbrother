<?php

namespace SK\OrganizationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class RoomAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General')
                ->add('name', null, array('label' => 'First Name', 'required' => true))
            ->end()
            ->with('ACS')
                ->add('accessControlSystemId', null, array('label' => 'ACS Id', 'required' => true))
                ->add('parent', 'sonata_type_model_list', array('label' => 'Parent Room', 'required' => false))
                ->add('children', 'sonata_type_model',
                                                    array(
                                                        'label' => 'Children Rooms',
                                                        'by_reference' => false,
                                                        'multiple' => true,
                                                        'expanded' => true,
                                                    ))
                ->add('checkpoints', 'sonata_type_model',
                                                    array(
                                                        'label' => 'Checkpoints',
                                                        'by_reference' => false,
                                                        'multiple' => true,
                                                        'expanded' => true,
                                                    ))
            ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('accessControlSystemId')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->addIdentifier('name')
            ->addIdentifier('checkpoints')
            ->addIdentifier('parent')
            ->addIdentifier('accessControlSystemId')
        ;
    }
}