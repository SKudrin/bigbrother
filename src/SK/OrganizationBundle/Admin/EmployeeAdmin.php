<?php

namespace SK\OrganizationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class EmployeeAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General')
                ->add('firstName', null, array('label' => 'First Name', 'required' => true))
                ->add('lastName', null, array('label' => 'Last Name', 'required' => true))
                ->add('middleName', null, array('label' => 'Middle Name', 'required' => true))
                ->add('department', null, array('label' => 'Department', 'required' => true))
            ->end()
            ->with('ACS')
                ->add('accessControlSystemId', null, array('label' => 'ACS Id','required' => true))
            ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('lastName')
            ->add('department')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->addIdentifier('lastName')
            ->addIdentifier('department')
            ->add('accessControlSystemId')
        ;
    }
}