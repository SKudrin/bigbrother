<?php

namespace SK\OrganizationBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CheckpointAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General')
                ->add('name', null, array('label' => 'First Name', 'required' => true))
            ->end()
            ->with('ACS')
                ->add('accessControlSystemId', null, array('label' => 'ACS Id', 'required' => true))
                // ->add('room', 'sonata_type_model_list', array('label' => 'Room', 'required' => false))
            ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('accessControlSystemId')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->addIdentifier('name')
            ->addIdentifier('room')
            ->addIdentifier('accessControlSystemId')
        ;
    }
}