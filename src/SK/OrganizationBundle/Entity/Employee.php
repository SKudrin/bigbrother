<?php

namespace SK\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Employee
 *
 * @ORM\Table(name="sk_organization_employee")
 * @ORM\Entity(repositoryClass="SK\OrganizationBundle\Entity\EmployeeRepository")
 */
class Employee
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="rc_organization_employee_id_seq", initialValue=1, allocationSize=1)
     */
    protected $id;

    /**
     * @ORM\Column(name="first_name", type="string", length=150, nullable=false)
     */
    protected $firstName;

    /**
     * @ORM\Column(name="middle_name", type="string", length=150, nullable=false)
     */
    protected $middleName;

    /**
     * @ORM\Column(name="last_name", type="string", length=150, nullable=false)
     */
    protected $lastName;

    /**
     * @ORM\Column(name="access_control_system_id", type="text", nullable=false)
     */
    protected $accessControlSystemId;

    /**
     * @ORM\Column(name="department", type="text", nullable=false)
     */
    protected $department;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Employee
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set middleName
     *
     * @param string $middleName
     * @return Employee
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Get middleName
     *
     * @return string 
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Employee
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set accessControlSystemId
     *
     * @param string $accessControlSystemId
     * @return Employee
     */
    public function setAccessControlSystemId($accessControlSystemId)
    {
        $this->accessControlSystemId = $accessControlSystemId;

        return $this;
    }

    /**
     * Get accessControlSystemId
     *
     * @return string 
     */
    public function getAccessControlSystemId()
    {
        return $this->accessControlSystemId;
    }

    /**
     * Set department
     *
     * @param string $department
     * @return Employee
     */
    public function setDepartment($department)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return string 
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * __toString()
     *
     * @return string 
     */
    public function __toString()
    {
        return $this->firstName . ' ' . $this->lastName;
    }
}
