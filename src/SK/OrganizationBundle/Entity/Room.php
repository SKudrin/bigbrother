<?php

namespace SK\OrganizationBundle\Entity;

use SK\OrganizationBundle\Entity\Checkpoint as Checkpoint;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Room
 *
 * @ORM\Table(name="rc_organization_room")
 * @ORM\Entity(repositoryClass="SK\OrganizationBundle\Entity\RoomRepository")
 */
class Room
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="rc_organization_room_id_seq", initialValue=1, allocationSize=1)
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="text", nullable=false)
     */
    protected $name;

    /**
     * @ORM\Column(name="access_control_system_id", type="text", nullable=false)
     */
    protected $accessControlSystemId;

    /**
     * @ORM\ManyToOne(targetEntity="Room", inversedBy="children")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    protected $parent;

    /**
     * @ORM\OneToMany(targetEntity="Room", mappedBy="parent")
     */
    protected $children;


    /**
     * @ORM\OneToMany(targetEntity="Checkpoint", mappedBy="room")
     */
    protected $checkpoints;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->checkpoints = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Room
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set accessControlSystemId
     *
     * @param string $accessControlSystemId
     * @return Room
     */
    public function setAccessControlSystemId($accessControlSystemId)
    {
        $this->accessControlSystemId = $accessControlSystemId;

        return $this;
    }

    /**
     * Get accessControlSystemId
     *
     * @return string 
     */
    public function getAccessControlSystemId()
    {
        return $this->accessControlSystemId;
    }

    /**
     * Set parent
     *
     * @param \SK\OrganizationBundle\Entity\Room $parent
     * @return Room
     */
    public function setParent(Room $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \SK\OrganizationBundle\Entity\Room 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add children
     *
     * @param \SK\OrganizationBundle\Entity\Room $children
     * @return Room
     */
    public function addChild(Room $children)
    {
        $this->children[] = $children;
        $children->setParent($this);

        return $this;
    }

    /**
     * Remove children
     *
     * @param \SK\OrganizationBundle\Entity\Room $children
     */
    public function removeChild(Room $children)
    {
        $this->children->removeElement($children);
        $children->setParent();

        return $this;
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add checkpoint
     *
     * @param \SK\OrganizationBundle\Entity\Checkpoint $checkpoint
     * @return Room
     */
    public function addCheckpoint(Checkpoint $checkpoint)
    {
        $this->checkpoints[] = $checkpoint;
        $checkpoint->setRoom($this);

        return $this;
    }

    /**
     * Remove checkpoint
     *
     * @param \SK\OrganizationBundle\Entity\Checkpoint $checkpoint
     */
    public function removeCheckpoint(Checkpoint $checkpoint)
    {
        $this->checkpoints->removeElement($checkpoint);
        $checkpoint->setRoom();

        return $this;
    }

    /**
     * Get checkpoints
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCheckpoints()
    {
        return $this->checkpoints;
    }

    /**
     * __toString()
     *
     * @return string 
     */
    public function __toString()
    {
        return $this->name;
    }
}
