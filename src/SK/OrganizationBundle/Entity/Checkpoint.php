<?php

namespace SK\OrganizationBundle\Entity;

use SK\OrganizationBundle\Entity\Room as Room;
use Doctrine\ORM\Mapping as ORM;

/**
 * Checkpoint
 *
 * @ORM\Table(name="rc_organization_checkpoint")
 * @ORM\Entity(repositoryClass="SK\OrganizationBundle\Entity\CheckpointRepository")
 */
class Checkpoint
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="rc_organization_checkpoint_id_seq", initialValue=1, allocationSize=1)
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="text", nullable=false)
     */
    protected $name;

    /**
     * @ORM\Column(name="access_control_system_id", type="text", nullable=false)
     */
    protected $accessControlSystemId;

    /**
     * @ORM\ManyToOne(targetEntity="Room", inversedBy="checkpoints")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="room_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    protected $room;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Checkpoint
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set accessControlSystemId
     *
     * @param string $accessControlSystemId
     * @return Checkpoint
     */
    public function setAccessControlSystemId($accessControlSystemId)
    {
        $this->accessControlSystemId = $accessControlSystemId;

        return $this;
    }

    /**
     * Get accessControlSystemId
     *
     * @return string 
     */
    public function getAccessControlSystemId()
    {
        return $this->accessControlSystemId;
    }

    /**
     * Set room
     *
     * @param \SK\OrganizationBundle\Entity\Room $room
     * @return Checkpoint
     */
    public function setRoom(Room $room = null)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return \SK\OrganizationBundle\Entity\Room 
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * __toString()
     *
     * @return string 
     */
    public function __toString()
    {
        return $this->name;
    }
}
