<?php

namespace SK\OrganizationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SKOrganizationBundle:Default:index.html.twig', array('name' => $name));
    }
}
