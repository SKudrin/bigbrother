<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140706170618 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("CREATE SEQUENCE rc_organization_checkpoint_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
        $this->addSql("CREATE SEQUENCE rc_organization_employee_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
        $this->addSql("CREATE SEQUENCE rc_organization_room_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
        $this->addSql("CREATE TABLE rc_organization_checkpoint (id INT NOT NULL, room_id INT DEFAULT NULL, name TEXT NOT NULL, access_control_system_id TEXT NOT NULL, PRIMARY KEY(id))");
        $this->addSql("CREATE INDEX IDX_4B4CD59554177093 ON rc_organization_checkpoint (room_id)");
        $this->addSql("CREATE TABLE sk_organization_employee (id INT NOT NULL, first_name VARCHAR(150) NOT NULL, middle_name VARCHAR(150) NOT NULL, last_name VARCHAR(150) NOT NULL, access_control_system_id TEXT NOT NULL, department TEXT NOT NULL, PRIMARY KEY(id))");
        $this->addSql("CREATE TABLE rc_organization_room (id INT NOT NULL, parent_id INT DEFAULT NULL, name TEXT NOT NULL, access_control_system_id TEXT NOT NULL, PRIMARY KEY(id))");
        $this->addSql("CREATE INDEX IDX_222BBC07727ACA70 ON rc_organization_room (parent_id)");
        $this->addSql("ALTER TABLE rc_organization_checkpoint ADD CONSTRAINT FK_4B4CD59554177093 FOREIGN KEY (room_id) REFERENCES rc_organization_room (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE rc_organization_room ADD CONSTRAINT FK_222BBC07727ACA70 FOREIGN KEY (parent_id) REFERENCES rc_organization_room (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE rc_organization_checkpoint DROP CONSTRAINT FK_4B4CD59554177093");
        $this->addSql("ALTER TABLE rc_organization_room DROP CONSTRAINT FK_222BBC07727ACA70");
        $this->addSql("DROP SEQUENCE rc_organization_checkpoint_id_seq CASCADE");
        $this->addSql("DROP SEQUENCE rc_organization_employee_id_seq CASCADE");
        $this->addSql("DROP SEQUENCE rc_organization_room_id_seq CASCADE");
        $this->addSql("DROP TABLE rc_organization_checkpoint");
        $this->addSql("DROP TABLE sk_organization_employee");
        $this->addSql("DROP TABLE rc_organization_room");
    }
}
